'''Deletes entries 1-by-1 from database. Designed to help clean up false entries from database.'''

from Main import *

def main():
	options_to_delete()
	delete_drink()

if __name__ == '__main__':
	main()
