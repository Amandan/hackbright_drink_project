# README #

# Repository Project Title: Drink Project #
Objective:  Create a visual picture of the sugar content in drinks sold in stores (that have nutrition fact labels).
It utilizes Tkinter GUIs, SqlAlchemy databases, and the turtle drawing program.

# Psuedocode #
Create a database of drinks and some of their key nutrition fact properties (SqlAlchemy)
Create a program that:
    1.  Finds a drink from the database
    2.  Or, allows the user to add a drink to the database
    3.  Calculates how much sugar is in the selected drink
    4.  Calculates a proxy measurement of that sugar (currently done using the nutrition facts of oreos)
Create a program that does all of the above using simple GUIs (currently done using Tkinter)
Create a program to visualize this proxy measurement of sugar pictorially on the screen (currently done using turtle)

# How do I get set up? #
1. Download all of the files (do NOT include the used_while_building directory or the test.db file)
2. pip install -r requirements.txt
3. Run data.py to set up the SqlAlchemy database
4. Run Main (for GUI version) or Terminal Main (for typed-in-terminal version)
    - for removal from the database, run delete_drink.py 
5. Help out the author by writing tests! (:
    - also, author needs to make sure "other" is always the last dropdown item in the OptionMenu; help appreciated

# Who do I talk to? #
* Repo owner (Amandan)