import turtle

oreo = None
# x = -270
# y = 200

# starts at the bottom, draws to the right
def draw_oreo():
	oreo_tuple = oreo.pos()
	x = oreo_tuple[0]
	y = oreo_tuple[1]
	oreo.goto(x, y)
	oreo.pencolor('black')
	oreo.circle(40)
	oreo.pencolor('white')
	y += 40
	oreo.goto(x, y)
	oreo.pencolor('black')
	oreo.write('Oreo', align='center', font=('Arial', 16, 'normal'))
	oreo.pencolor('white')
	x += 90
	y += -40
	oreo.goto(x, y)

def draw_total(num_oreos):
	oreo_tuple = oreo.pos()
	x = oreo_tuple[0]
	y = oreo_tuple[1]
	oreo.goto(x, y)
	i = 1
	while i <= num_oreos:
		if i > 1 and (i-1)%7 == 0:
			x += -630
			y += -100
			oreo.goto(x,y)
		draw_oreo()
		i += 1
	# print oreo.position()

def move_down_to_write_box():
	oreo_tuple = oreo.pos()
	x = -70
	y = oreo_tuple[1]
	oreo.pencolor('white')
	y += -100
	oreo.goto(x, y)
	oreo.pencolor('#0E9C02')

def move_down_to_write_alt():
	oreo_tuple = oreo.pos()
	x = -70
	y = oreo_tuple[1]
	oreo.pencolor('white')
	y += -100
	oreo.goto(x, y)
	oreo.pencolor('#0060A6')

def move_down_line_two():
	oreo_tuple = oreo.pos()
	x = -70
	y = oreo_tuple[1]
	oreo.pencolor('white')
	y += -20
	oreo.goto(x, y)
	oreo.pencolor('#0060A6')

def percent_oreo_box(num_oreos):
	if 15 >= num_oreos >= 10:
		move_down_to_write_box()
		oreo.write("That's a third of the box!", align='center', font=('Arial', 18, 'normal'))
	elif 18 > num_oreos >= 15:
		move_down_to_write_box()
		oreo.write("That's almost half of the box!", align='center', font=('Arial', 18, 'normal'))
	elif num_oreos == 18:
		move_down_to_write_box()
		oreo.write("That's half of the box!", align='center', font=('Arial', 18, 'normal'))
	elif 36 >= num_oreos >= 19:
		move_down_to_write_box()
		oreo.write("That's more than half of the box!", align='center', font=('Arial', 18, 'normal'))

def main(num_oreos, alt_drink):
	global oreo
	oreo = turtle.Turtle()
	oreo.hideturtle()
	oreo.speed('fastest')
	oreo.pencolor('white')
	oreo.goto(-270, 200)

	draw_total(num_oreos)
	percent_oreo_box(num_oreos)
	if float(num_oreos) > 2.5:
		move_down_to_write_alt()
		if len(alt_drink) > 20:
			oreo.write('Why not try another low-sugar beverage like ', align='center', font=('Arial', 18, 'normal'))
			move_down_line_two()
			oreo.write(alt_drink + '?', align='center', font=('Arial', 18, 'normal'))
		else:	
			oreo.write('Why not try another low-sugar beverage like ' + alt_drink + '?', align='center', font=('Arial', 18, 'normal'))
	if float(num_oreos) < 2.5:
		move_down_to_write_alt()
		oreo.write('Great low-sugar drink!', align='center', font=('Arial', 18, 'normal'))

	raw_input('press enter to continue > ')
	turtle.bye()


if __name__ == '__main__':
	main()