from sqlalchemy.engine import create_engine
from sqlalchemy import schema, types, orm, Column, Integer, Float, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select
from data import *
import turtle_oreo

engine = create_engine('sqlite:///test.db', echo=False)
metadata = schema.MetaData(bind=engine, reflect=False)
Base = declarative_base()


class Drink(Base):
	__tablename__ = 'drinks'
	id = Column(Integer, primary_key=True)
	drink_name = Column(String, unique=True)
	sugar = Column(Float)
	serving_size = Column(String)
	num_servings = Column(Float)

	def __str__(self):
		return self.drink_name + " " + str(self.sugar) 


def drink_selector(drink_session):
	s = select([Drink])
	result = data.drink_session.execute(s)
	for item in result:
		print item.drink_name
	selection = raw_input("\nWhich drink do you want to visualize:").lower()
	s = select([Drink]).where(Drink.drink_name == selection)
	result = data.drink_session.execute(s)
	for item in result:
		return item
	# print "\nThat's not on the list. Try again"
	
	print "\nThat's not on the list. Was that a typo, or do you want me to add your drink to the database?  "
	user_input = raw_input("Type 'typo' or 'add':  ").lower()
	if user_input == 'typo':
		return drink_selector(data.drink_session)
	elif user_input == 'add':
		add_drink(data.drink_session)
		return drink_selector(data.drink_session)
	else:
		print "I didn't understand that."
		return drink_selector(data.drink_session)
	# selection = raw_input("Which drink do you want to visualize:\nCoke\nCoconut Water\nGatorade 20oz\nGatorade 32oz\nOrange Juice?")


def add_drink(drink_session):
	name = raw_input("What's the full name of the drink?  ").lower()
	if len(name) < 1:
		print "Full name is required\n"
		drink_selector(data.drink_session)
	# nick_name = raw_input("Does it have another name?").lower()
	# if nick_name == no:
	# 	pass
	sugar = raw_input("How many grams of sugar per serving does it have?\nJust type the number (without the 'g')  ").lower()
	sug = float(sugar)
	if len(name) < 1:
		print "Sugar is required\n"
		drink_selector(data.drink_session) 
	serving_size = raw_input("What is the serving size?\nInclude units, e.g. '8fl oz')  ").lower()
	if len(name) < 1:
		print "Number of servings is required\n"
		drink_selector(data.drink_session) 
	num_servings = raw_input("How many servings are in the drink?  ").lower()
	num_servs = float(num_servings)
	if len(name) < 1:
		print "Number of servings is required\n"
		drink_selector(data.drink_session) 
	name = Drink(drink_name=name, sugar=sug, serving_size=serving_size, num_servings=num_servs)
	data.drink_session.add(name)
	data.drink_session.commit()


def sugar_per_drink(drink):
	total_sugar = drink.sugar * drink.num_servings
	return total_sugar

def total_oreos(total_sugar):
	return total_sugar / 3.5

		
def main():
	selected_obj = drink_selector(data.drink_session)
	flt_num = sugar_per_drink(selected_obj)
	to_visualize = int(round(flt_num))
	o = total_oreos(to_visualize)
	turtle_oreo.main(o)



	# selection = raw_input("\nWhich drink do you want to visualize:").lower()
	# s = drink_session.query(Drink).filter(Drink.drink_name==selection).first()
	# s.drink_name = 'corrected name'
	# or:   session.delete(s)
	# 		session.commit()

if __name__ == '__main__':
	main()
