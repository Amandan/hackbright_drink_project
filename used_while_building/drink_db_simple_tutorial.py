from sqlalchemy.engine import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import schema, types, orm, Column, Integer, Float, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import select
# import turtle_oreo

engine = create_engine('sqlite:///test.db', echo=False)
# metadata = schema.MetaData(bind=engine, reflect=False)
Base = declarative_base()
# Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
drink_session = Session()


class Drink(Base):
	__tablename__ = 'drinks'
	id = Column(Integer, primary_key=True)
	drink_name = Column(String, unique=True)
	# nick_name = Column(String)
	sugar = Column(Float)
	serving_size = Column(String)
	num_servings = Column(Float)

	def __str__(self):
		return self.drink_name + " " + str(self.sugar) 

	# def sugar_per_drink(self):
	# 	total_sugar = self.sugar * self.num_servings
	# 	return total_sugar

	# def visualize_oreos(self):
	# 	num_oreos = self.sugar_per_drink() / 3.5
	# 	print num_oreos
	# 	if 15 >= num_oreos >= 12:
	# 		print "That's a third of the box"
	# 	elif 18 > num_oreos >= 15:
	# 		print "That's almost half of the box"
	# 	elif num_oreos == 18:
	# 		print "That's half of the box"
	# 	elif 36 >= num_oreos >= 19:
	# 		print "That's more than half of the box"
	# 	return num_oreos

def drink_selector(drink_session):
	s = select([Drink])
	result = drink_session.execute(s)
	for item in result:
		print item.drink_name
	selection = raw_input("\nWhich drink do you want to visualize:").lower()
	s = select([Drink]).where(Drink.drink_name == selection)
	result = drink_session.execute(s)
	for item in result:
		return item
	# print "\nThat's not on the list. Try again"
	
	print "\nThat's not on the list. Was that a typo, or do you want me to add your drink to the database?  "
	user_input = raw_input("Type 'typo' or 'add':  ").lower()
	if user_input == 'typo':
		return drink_selector(drink_session)
	elif user_input == 'add':
		add_drink(drink_session)
		return drink_selector(drink_session)
	else:
		print "I didn't understand that."
		return drink_selector(drink_session)
	# selection = raw_input("Which drink do you want to visualize:\nCoke\nCoconut Water\nGatorade 20oz\nGatorade 32oz\nOrange Juice?")


def add_drink(drink_session):
	name = raw_input("What's the full name of the drink?  ").lower()
	if len(name) < 1:
		print "Full name is required\n"
		drink_selector(drink_session)
	# nick_name = raw_input("Does it have another name?").lower()
	# if nick_name == no:
	# 	pass
	sugar = raw_input("How many grams of sugar per serving does it have?\nJust type the number (without the 'g')  ").lower()
	sug = float(sugar)
	if len(name) < 1:
		print "Sugar is required\n"
		drink_selector(drink_session) 
	serving_size = raw_input("What is the serving size?\nInclude units, e.g. '8fl oz')  ").lower()
	if len(name) < 1:
		print "Number of servings is required\n"
		drink_selector(drink_session) 
	num_servings = raw_input("How many servings are in the drink?  ").lower()
	num_servs = float(num_servings)
	if len(name) < 1:
		print "Number of servings is required\n"
		drink_selector(drink_session) 
	name = Drink(drink_name=name, sugar=sug, serving_size=serving_size, num_servings=num_servs)
	drink_session.add(name)
	drink_session.commit()


def sugar_per_drink(drink):
	total_sugar = drink.sugar * drink.num_servings
	return total_sugar

def total_oreos(total_sugar):
	return total_sugar / 3.5

		
def main():


	coke = Drink(drink_name='coca cola', sugar=39, serving_size='8fl oz', num_servings=1)
	drink_session.add(coke)

	coconut_water = Drink(drink_name='coconut water', sugar=11, serving_size='8fl oz', num_servings=2)
	drink_session.add(coconut_water)

	gatorade_20 = Drink(drink_name='gatorade 20oz', sugar=35, serving_size='20', num_servings=1)
	drink_session.add(gatorade_20)

	gatorade_32 = Drink(drink_name='gatorade 32oz', sugar=14, serving_size='8fl oz', num_servings=4)
	drink_session.add(gatorade_32)

	orange_juice = Drink(drink_name='orange juice', sugar=21, serving_size='8fl oz', num_servings=1)
	drink_session.add(orange_juice)

	drink_session.commit()


	# selected_obj = drink_selector(drink_session)
	# flt_num = sugar_per_drink(selected_obj)
	# to_visualize = int(round(flt_num))
	# o = total_oreos(to_visualize)
	# turtle_oreo.main(o)


	# print type(coke)
	# coke.visualize_oreos
	# coconut_water.visualize_oreos()
	# gatorade_20.visualize_oreos()
	# gatorade_32.visualize_oreos()
	# orange_juice.visualize_oreos()
	# print coke.drink_name, coke.sugar

if __name__ == '__main__':
	main()
