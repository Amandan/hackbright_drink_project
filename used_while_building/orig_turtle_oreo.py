import turtle
# import SqlAlch_setup_dp

# turtle.cfg.title = 'How Many Oreos are in your Drink?'

num_oreos = int(raw_input("How many oreos should I draw?"))
oreo = turtle.Turtle()
x = -260
y = 200
# oreo.pencolor('white')

# starts at the bottom, draws to the right
def draw_oreo():
  global x
  global y
  oreo.goto(x, y)
  oreo.pencolor('black')
  oreo.circle(40)
  # oreo.pencolor('white')
  y += 40
  oreo.goto(x, y)
  oreo.pencolor('black')
  oreo.write('Oreo', True, align='center', font=('Arial', 16, 'normal'))
  # oreo.pencolor('white')
  x += 90
  y += -40
  oreo.goto(x, y)

draw_oreo()
draw_oreo()
draw_oreo()

turtle.done()

window.exitonclick()