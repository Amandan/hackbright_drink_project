import turtle
x = -270
y = 200

#process of drawing 1 oreo:
def draw_oreo(oreo):
	global x
	global y
	oreo.goto(x, y)
	oreo.pencolor('black')
	oreo.circle(40)
	oreo.pencolor('white')
	y += 40
	oreo.goto(x, y)
	oreo.pencolor('black')
	oreo.write('Oreo', align='center', font=('Arial', 16, 'normal'))
	oreo.pencolor('white')
	x += 90
	y += -40
	oreo.goto(x, y)

# draws all oreos calculated in your drink:
# (note: starts at the top left corner, draws to the right. for numbers over 7 or multiples of, moves to next line, continues drawing.)
def draw_total(num_oreos, oreo):
	global x
	global y
	i = 1
	while i <= num_oreos:
		if i > 1 and (i-1)%7 == 0:
			x += -630
			y += -100
			oreo.goto(x,y)
		draw_oreo(oreo)
		i += 1

# invisible movement for text messages:
def move_down_to_write_box(oreo):
	global y
	oreo.pencolor('white')
	x = -70
	y += -100
	oreo.goto(x, y)
	oreo.pencolor('#0E9C02')

# invisible movement for text messages:
def move_down_to_write_alt(oreo):
	global y
	oreo.pencolor('white')
	x = -70
	y += -100
	oreo.goto(x, y)
	oreo.pencolor('#0060A6')

# invisible movement for text messages that are more than 1 line long:
def move_down_line_two(oreo):
	global y
	oreo.pencolor('white')
	x = -70
	y += -20
	oreo.goto(x, y)
	oreo.pencolor('#0060A6')

# calculates the first message to be written
#(if your drink contains over 10 sugar-oreos in it):
def percent_oreo_box(num_oreos, oreo):
	if 15 >= num_oreos >= 10:
		move_down_to_write_box(oreo)
		oreo.write("That's a third of the box!", align='center', font=('Arial', 18, 'normal'))
	elif 18 > num_oreos >= 15:
		move_down_to_write_box(oreo)
		oreo.write("That's almost half of the box!", align='center', font=('Arial', 18, 'normal'))
	elif num_oreos == 18:
		move_down_to_write_box(oreo)
		oreo.write("That's half of the box!", align='center', font=('Arial', 18, 'normal'))
	elif 36 >= num_oreos >= 19:
		move_down_to_write_box(oreo)
		oreo.write("That's more than half of the box!", align='center', font=('Arial', 18, 'normal'))

# selects the second message to be written
# (either an alternative suggestion to your drink, or a congratulations on low-sugar content):
def alt_drink_suggestion(num_oreos, alt_drink, oreo):
	if float(num_oreos) > 2.5:
		move_down_to_write_alt(oreo)
		if len(alt_drink) > 20:
			oreo.write('Why not try another low-sugar beverage like ', align='center', font=('Arial', 18, 'normal'))
			move_down_line_two(oreo)
			oreo.write(alt_drink + '?', align='center', font=('Arial', 18, 'normal'))
		else:	
			oreo.write('Why not try another low-sugar beverage like ' + alt_drink + '?', align='center', font=('Arial', 18, 'normal'))
	if float(num_oreos) < 2.5:
		if float(num_oreos) < 1:
			move_down_to_write_alt(oreo)
			oreo.write("There's no sugar in that drink. Great option!", align='center', font=('Arial', 18, 'normal'))
		else:
			move_down_to_write_alt(oreo)
			oreo.write("That's a great low-sugar drink!", align='center', font=('Arial', 18, 'normal'))

def main(num_oreos, alt_drink):
	wn = turtle.Screen()
	wn.title('How Many Oreos are in your Drink?')

	oreo = turtle.Turtle()
	oreo.hideturtle()
	oreo.speed('fastest')

	oreo.pencolor('white')
	draw_total(num_oreos, oreo)
	percent_oreo_box(num_oreos, oreo)
	alt_drink_suggestion(num_oreos, alt_drink, oreo)

	turtle.exitonclick()

#if running this file directly for testing purposes:
if __name__ == '__main__':
	main(20, 'water')