from sqlalchemy.engine import create_engine
from sqlalchemy import schema, types, orm, Column, Integer, Float, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select
import turtle_oreo
from data import Drink
import random
import Tkinterface

engine = create_engine('sqlite:///test.db', echo=False)
metadata = schema.MetaData(bind=engine, reflect=False)
Base = declarative_base()
Session = sessionmaker(bind=engine)
drink_session = Session()

"""This is the main file of the Drink Project.  It visualizes how much sugar is in drinks in its database.
To do this, it calls a GUI file (Tkinterface) and an Oreo Visualization (turtle_oreo) file."""

# selects the drink in the database that you typed in terminal (used in Terminal_Main):
def drink_selector(drink_session):
	s = select([Drink])
	result = drink_session.execute(s)
	for item in result:
		print item.drink_name
	selection = raw_input("\nWhich drink do you want to visualize?:  ").lower()
	if selection == 'exit':
		exit()
	s = select([Drink]).where(Drink.drink_name == selection)
	result = drink_session.execute(s)
	for item in result:
		return item
	
	print "\nThat's not on the list. Was that a typo, or do you want me to add your drink to the database?  "
	user_input = raw_input("Type 'typo' or 'add':  ").lower()
	if user_input == 'typo':
		return drink_selector(drink_session)
	elif user_input == 'add':
		add_drink(drink_session)
		return drink_selector(drink_session)
	else:
		print "I didn't understand that."
		return drink_selector(drink_session)

# adds the drink and drink info you typed in terminal (used in Terminal_Main):
def add_drink(drink_session):
	name = raw_input("What's the full name of the drink?  ").lower()
	if name == 'exit':
		exit()
	if len(name) < 1:
		print "Full name is required\n"
		return drink_selector(drink_session)
	sug = None
	while sug is None:
		sugar = raw_input("How many grams of sugar per serving does it have?\nJust type the number (without the 'g')  ").lower()
		try:
			sug = float(sugar)
		except ValueError:
			print "Hey! That's not a number. Remember: I don't need the units (e.g. grams)"
	if len(name) < 1:
		print "Sugar is required\n"
		return drink_selector(drink_session) 
	serving_size = raw_input("What is the serving size?\nInclude units, e.g. '8fl oz')  ").lower()
	if len(name) < 1:
		print "Number of servings is required\n"
		return drink_selector(drink_session) 
	num_servings = raw_input("How many servings are in the drink?  ").lower()
	num_servs = float(num_servings)
	if len(name) < 1:
		print "Number of servings is required\n"
		return drink_selector(drink_session) 
	name = Drink(drink_name=name, sugar=sug, serving_size=serving_size, num_servings=num_servs, alt_cat=categorize(name))
	drink_session.add(name)
	drink_session.commit()

# adds drink and drink info you entered into GUI (Tkinterface):
def add_drink_AddDrink(new_drink_list):
	global drink_session
	name = Drink(drink_name=new_drink_list[0], sugar=new_drink_list[1], serving_size=new_drink_list[2], num_servings=new_drink_list[3], alt_cat=categorize(new_drink_list[0]))
	drink_session.add(name)
	drink_session.commit()

# categorizes drink you add them to database:
def categorize(name):
	if 'juice' in name:
		return 'fruit'
	elif 'water' in name:
		return 'fruit'
	elif 'tea' in name:
		return 'caffeine'
	elif 'coffee' in name:
		return 'caffeine'
	elif 'cafe' in name:
		return 'caffeine'
	elif 'soda' in name:
		return caffiene
	else:
		return None	

def options_to_delete():
	s = select([Drink])
	result = drink_session.execute(s)
	for item in result:
		print item.drink_name

def delete_drink():
	name = raw_input("What's the name of the drink you want to remove?  ").lower()
	exit_commands = ['exit', 'cancel', 'done', 'no', 'n']
	for item in exit_commands:
		if name == item:
			return exit()
	if len(name) < 1:
		print "Full name is required\n"
		return exit()
	result = drink_session.query(Drink).filter(Drink.drink_name == name).delete()
	drink_session.commit()

# calculates the sugar in your selected drink:
def sugar_per_drink(drink):
	total_sugar = drink.sugar * drink.num_servings
	return total_sugar

# calculates the number of oreos (by sugar) that are in your selected drink:
def total_oreos(total_sugar):
	return total_sugar / 3.5

# selects an alternative drink option to pass to the turtle_oreo visualization file:
def random_drink(selected_obj):
	if selected_obj.alt_cat == "hydration":
		hydration_list = ["water", "unsweeteened flavored water", "adding a fruit juice 'shot' to a glass of water"]
		return random.choice(hydration_list)
	elif selected_obj.alt_cat == "fruit":
		fruit_list = ["water", "unsweeteened flavored water", "adding a fruit juice 'shot' to a glass of water", "unsweeteened flavored seltzer water"]
		return random.choice(fruit_list)
	else:
		caffeine_list = ["unsweetened green tea", "unsweetened iced black tea", "iced coffee", "unsweeteened flavored seltzer water", "kombucha"]
		return random.choice(caffeine_list)

#creates a list for the GUI OptionMenu box of the drinks in the database:
def dropdown_drink_list(drink_session):
	dropdown_list = []
	s = select([Drink])
	result = drink_session.execute(s)
	for item in result:
		dropdown_list.append(item.drink_name)
	return dropdown_list

# locates the drink object chosen from the GUI OptionMenu box in the database:
def selecting_from_session(dropdown_value):
	global drink_session
	s = select([Drink]).where(Drink.drink_name == dropdown_value)
	result = drink_session.execute(s)
	for item in result:
		drink = item
		return drink

# when GUI ok button is pushed, calculates number of oreos in your selected drink and passes that to the turtle_oreo visualization file:
def ok_btn(dropdown_value):
	dropdown_value = str(dropdown_value)
	drink = selecting_from_session(dropdown_value)
	flt_num = sugar_per_drink(drink)
	to_visualize = int(round(flt_num))
	o = total_oreos(to_visualize)
	alt_drink = random_drink(drink)
	turtle_oreo.main(o, alt_drink)

def main():
	global drink_session
	drinks = dropdown_drink_list(drink_session) # fetch drink names from db and turn into a list of strings
	first_drink = drinks[1]  #didn't want water to be the default drink visible in the dropdown
	root_widget = Tkinterface.buildUI(drinks, first_drink, ok_btn, Tkinterface.buildOptionalUI, add_drink_AddDrink)
	root_widget.mainloop()

if __name__ == '__main__':
	main()

