'''Sets up the SqlAlchemy database for Sugar_in_Drink_Project'''

from sqlalchemy.engine import create_engine
from sqlalchemy import schema, types, orm, Column, Integer, Float, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
	
engine = create_engine('sqlite:///test.db', echo=False)
metadata = schema.MetaData(bind=engine, reflect=False)
Base = declarative_base()

class Drink(Base):
	__tablename__ = 'drinks'
	id = Column(Integer, primary_key=True)
	drink_name = Column(String, unique=True)
	sugar = Column(Float)
	serving_size = Column(String)
	num_servings = Column(Float)
	alt_cat = Column(String)

	def __str__(self):
		return self.drink_name + " " + str(self.sugar)

def main():

	Base.metadata.create_all(engine)
	Session = sessionmaker(bind=engine)
	drink_session = Session()

#adding example data to the database:
	water = Drink(drink_name='water', sugar=0, serving_size='8fl oz', num_servings=1, alt_cat='hydration')
	drink_session.add(water)

	coke = Drink(drink_name='coca cola', sugar=39, serving_size='12fl oz', num_servings=1, alt_cat='caffeine')
	drink_session.add(coke)

	coconut_water = Drink(drink_name='coconut water', sugar=11, serving_size='8fl oz', num_servings=2, alt_cat='hydration')
	drink_session.add(coconut_water)

	gatorade_20 = Drink(drink_name='gatorade 20oz', sugar=35, serving_size='20fl oz', num_servings=1, alt_cat='hydration')
	drink_session.add(gatorade_20)

	gatorade_32 = Drink(drink_name='gatorade 32oz', sugar=14, serving_size='8fl oz', num_servings=4, alt_cat='hydration')
	drink_session.add(gatorade_32)

	orange_juice = Drink(drink_name='orange juice', sugar=21, serving_size='8fl oz', num_servings=1, alt_cat='fruit')
	drink_session.add(orange_juice)

	sprite = Drink(drink_name='sprite', sugar=38, serving_size='12fl oz', num_servings=1, alt_cat='fruit')
	drink_session.add(sprite)

	low_sugar_kombucha = Drink(drink_name="gt's synergy kombucha", sugar=2, serving_size='8fl oz', num_servings=2, alt_cat='fruit')
	drink_session.add(low_sugar_kombucha)

	# other = Drink(drink_name='other')
	# drink_session.add(other)

	drink_session.commit()

if __name__ == '__main__':
	main()