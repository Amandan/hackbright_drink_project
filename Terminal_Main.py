from sqlalchemy.engine import create_engine
from sqlalchemy import schema, types, orm, Column, Integer, Float, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select
import turtle_oreo
from data import Drink
import random
import Tkinterface
from Main import *

'''Same functionality as Main for Sugar_in_Drink_Project but in terminal without a GUI'''

def main():
	engine = create_engine('sqlite:///test.db', echo=False)
	metadata = schema.MetaData(bind=engine, reflect=False)
	Base = declarative_base()
	Session = sessionmaker(bind=engine)
	drink_session = Session()
	selected_obj = drink_selector(drink_session)
	flt_num = sugar_per_drink(selected_obj)
	to_visualize = int(round(flt_num))
	o = total_oreos(to_visualize)
	alt_drink = random_drink(selected_obj)
	turtle_oreo.main(o, alt_drink)

if __name__ == '__main__':
	main()