'''Builds the GUIs for Drink Project Selector and Add capabilities'''

from Tkinter import *
from Main import *

#GUI to select drinks from dropdown list
class DrinkBox(object):
	def __init__(self, master, drinks, first_drink, ok_function, buildOptionalUI, add_drink_AddDrink):
		self.master = master
		self.drinks = drinks
		self.first_drink = first_drink
		self.ok_function = ok_function
		self.buildOptionalUI = buildOptionalUI
		self.add_drink_AddDrink = add_drink_AddDrink
		self.buildUI = buildUI

		lbl_instructions = Label(master, text="What drink would you like to see?").grid(row=0, column=0)
		self.var = StringVar(master)
		self.var.set(first_drink)
		option = OptionMenu(master, self.var, *drinks).grid(row=1, column=0) 
		self.btn_ok = Button(master, text='OK', command=self.choice).grid(row=2, column=0)  

	def choice(self):
		temp_drink = self.var.get()
		if temp_drink == 'other':
			self.master.destroy()
			return buildOptionalUI(self.drinks, self.first_drink, self.ok_function, self.buildOptionalUI, self.add_drink_AddDrink)
		self.master.destroy()
		return self.ok_function(temp_drink)

#GUI to add drink not yet on dropdown list
class AddBox(object):
	global root_widget
	def __init__(self, master, drinks, first_drink, ok_function, buildOptionalUI, add_drink_AddDrink):
		self.master = master
		self.drinks = drinks
		self.first_drink = first_drink
		self.ok_function = ok_function
		self.buildOptionalUI = buildOptionalUI
		self.add_drink_AddDrink = add_drink_AddDrink

		#Add new drink GUI text input box labels:
		empty_row = Label(master, text='').grid(row=0, column=0, sticky=W)
		master_lbl = Label(master, text='ADD A NEW DRINK:').grid(row=1, column=0, sticky=W)
		empty_row2 = Label(master, text='').grid(row=2, column=0, sticky=W)
		drink_name = Label(master, text='Drink Name').grid(row=3, column=0, sticky=W)
		sugar = Label(master, text='How many grams of sugar?').grid(row=4, column=0, sticky=W)
		serving_size = Label(master, text='How much is the serving size?').grid(row=5, column=0, sticky=W)
		num_servings = Label(master, text='How many servings in the bottle?').grid(row=6, column=0, sticky=W)

		# New drink name text input box:
		self.entry_text = Entry(master)
		self.entry_text.insert(0, "name")
		self.entry_text.grid(row=3, column=1, columnspan=2, sticky=E)  

		# New drink sugar text input box:
		self.entry_text2 = Entry(master)                 
		self.entry_text2.insert(0, "only type numbers")
		self.entry_text2.grid(row=4, column=1, columnspan=2, sticky=E)   

		# New drink serving size text input box:
		self.entry_text3 = Entry(master)                        
		self.entry_text3.insert(0, "include units like 'fl oz'")
		self.entry_text3.grid(row=5, column=1, columnspan=2, sticky=E)

		# New drink number of servings text input box:
		self.entry_text4 = Entry(master)                        
		self.entry_text4.insert(0, "only type numbers")
		self.entry_text4.grid(row=6, column=1, columnspan=2, sticky=E)

		#OK and Cancel buttons:
		self.btn_ok2 = Button(master, text='OK', command=self.clicked_ok).grid(row=7, column=1)
		self.btn_cancel = Button(master, text='Cancel', command=self.clicked_cancel).grid(row=7, column=2)

	#OK button:  Adds drink to database and then visualizes with Turtle Oreo program
	def clicked_ok(self):
		global root_widget
		new_drink = self.entry_text.get()
		new_sugar = self.entry_text2.get()
		new_serv_size = self.entry_text3.get()
		new_servings = self.entry_text4.get()
		new_drink_list = [new_drink.lower(), float(new_sugar), new_serv_size.lower(), float(new_servings)]
		self.master.destroy()
		self.add_drink_AddDrink(new_drink_list)
		return self.ok_function(new_drink_list[0])
		# Or: return original widget for user to choose from
		# (note: have not yet fixed the refresh of the list. the drink you just added will not be included...)
			# self.master = Tk()
			# self.master.geometry("250x100") 
			# self.master.title("Sugar Goggles")
			# app = DrinkBox(self.master, self.drinks, self.first_drink, self.ok_function, self.buildOptionalUI, self.add_drink_AddDrink)
			# return self.master

	#Cancel button:  Exits program
	def clicked_cancel(self):
		return exit()

#Tkinterface Geometry function is not 'smart'; can't pass in parameters...
	# def create_widget(w, h):
	# 	widget = Tk() 
	# 	widget.geometry(("%dx%d" % (w,h)))           
	# 	widget.title("Sugar Goggles")

def buildUI(drinks, first_drink, ok_function, buildOptionalUI, add_drink_AddDrink):
	global root_widget
	root_widget = Tk()                          # creates a widget window which will hold all other widgets
	root_widget.geometry("250x100")            
	root_widget.title("Sugar Goggles")        	
	app = DrinkBox(root_widget, drinks, first_drink, ok_function, buildOptionalUI, add_drink_AddDrink)
	return root_widget

def buildOptionalUI(drinks, first_drink, ok_function, buildOptionalUI, add_drink_AddDrink):
	optional_widget = Tk()                  
	optional_widget.geometry("450x230")             
	optional_widget.title("Sugar Goggles") 
	app = AddBox(optional_widget, drinks, first_drink, ok_function, buildOptionalUI, add_drink_AddDrink)
	return optional_widget

def main():
	drinks = ['coke', 'sprite', 'water']
	widget = buildUI(drinks)
	widget.mainloop()


if __name__ == '__main__':
	main()